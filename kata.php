<?php

function maxNumber($n) {
  $max_number_array = str_split($n); // int to array as digit
  rsort($max_number_array); // sort descending by value
  $max_number_print = intval(implode("", $max_number_array));

  return $max_number_print;
}

echo maxNumber(903941);